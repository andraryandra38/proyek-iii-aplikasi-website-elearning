-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Feb 2022 pada 07.53
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `exercise`
--

CREATE TABLE `exercise` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mapel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_exercise` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bataswaktu` datetime DEFAULT NULL,
  `user_id_teacher` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `exercise`
--

INSERT INTO `exercise` (`id`, `mapel`, `kelas`, `nama_exercise`, `deskripsi`, `file`, `bataswaktu`, `user_id_teacher`, `created_at`, `updated_at`) VALUES
(1, 'Pemprograman Berorientasi Objek', 'XI RPL 1', 'Tipe Data', 'HAYO EXERCISE NIH! ^o^', '', NULL, 2, NULL, NULL),
(2, 'Matematika', 'VII-G', 'Bangun Ruang', 'Ulangan Harian Bab 1', '', NULL, 2, '2021-12-09 08:20:22', '2021-12-09 08:20:22'),
(3, 'Pemprograman Berorientasi Objek', 'VII-G', 'tugas pbo', 'y', '1639389538_modul6krn.docx', NULL, 2, '2021-12-13 02:58:58', '2021-12-13 02:58:58'),
(4, 'Matematika', 'VII-G', 'hshsgsadhj', 'dadddw', '1639393093_1639293680_materi(1).pdf', NULL, 2, '2021-12-13 03:58:13', '2021-12-13 03:58:13'),
(5, 'Bahasa Indonesia', 'VII-G', 'Puisi', 'Tugas Bab Puisi', '1639961653_1639389538_modul6krn.docx', NULL, 2, '2021-12-20 00:54:13', '2021-12-20 00:54:13'),
(6, 'Bahasa Inggris', 'VII-G', 'Introduce', 'sddsadsa', '1639961959_class diagram (3) (1).jpg', '2021-12-20 10:00:00', 2, '2021-12-20 00:59:19', '2021-12-20 00:59:19'),
(7, 'Bahasa Indramayu', 'VII-A', 'Pitakon', 'uagdeabhjvbd', '1639984575_codinganiot.docx', '2021-12-20 15:00:00', 2, '2021-12-20 07:16:15', '2021-12-20 07:16:15'),
(8, 'Bahasa Indonesia', 'VIII-G', 'Polinomial', 'Polinomial', 'C:\\xampp\\tmp\\php789C.tmp', '2021-12-20 21:00:00', 30, '2021-12-20 11:16:21', '2021-12-20 11:21:17'),
(9, 'Bahasa Indonesia', 'VIII-G', 'Bangun Ruang', 'gdychgvgv', '1640001729_1639393093_1639293680_materi(1).pdf', '2021-12-29 21:00:00', 30, '2021-12-20 12:02:09', '2021-12-20 12:02:09'),
(10, 'Bahasa Indonesia', 'VII-A', 'Puisi', 'asjdhvashfd', '1640062671_modul6krn.docx', '2021-12-21 23:00:00', 30, '2021-12-21 04:57:52', '2021-12-21 04:57:52'),
(11, 'Matematika', 'VIII-G', 'Polinomial', 'vvsvddhav', '1640062959_modul6krn.docx', '2021-12-23 10:00:00', 30, '2021-12-21 05:02:40', '2021-12-21 05:02:40'),
(12, 'Matematika', 'VIII-A', 'Bangun Ruang', 'gvcgvvg g', '1640142828_codinganiot.docx', '2021-12-28 10:00:00', 30, '2021-12-22 03:13:48', '2021-12-22 03:13:48'),
(13, 'Seni Budaya', 'IX-A', 'bernyanyii', 'apa aja', '1640151371_1640062959_modul6krn.docx', '2021-12-22 20:20:00', 30, '2021-12-22 05:36:11', '2021-12-22 05:36:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jawaban_tugas`
--

CREATE TABLE `jawaban_tugas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_mapel` bigint(20) UNSIGNED NOT NULL,
  `id_exercise` bigint(20) UNSIGNED NOT NULL,
  `isi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id_student` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jawaban_tugas`
--

INSERT INTO `jawaban_tugas` (`id`, `id_mapel`, `id_exercise`, `isi`, `file`, `user_id_student`, `created_at`, `updated_at`) VALUES
(1, 2, 4, 'ghsdavdsavjhjvhdsv', 'vhhdsvhdsh', 3, NULL, NULL),
(2, 4, 4, '<p>2</p>', '1639902455_1639393093_1639293680_materi(1).pdf', 28, NULL, NULL),
(3, 2, 2, '<p>hhgjhgjgjh</p>', '1639976203_Usecase.drawio.png', 28, NULL, NULL),
(4, 5, 5, '<p>ffasff</p>', '1639976841_1639393093_1639293680_materi(1).pdf', 28, NULL, NULL),
(5, 7, 7, '<p>dadsasdsa</p>', '1639984698_codinganiot.docx', 38, NULL, NULL),
(6, 8, 8, '<p>jhdsajhgjhsdajgh</p>', '1639999330_1639393093_1639293680_materi(1).pdf', 50, NULL, NULL),
(7, 11, 11, '<p>jsajdhjsadsa</p>', '1640063013_codinganiot.docx', 56, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kelas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id`, `nama_kelas`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'VII-A', 'Kelas VII-A', NULL, '2021-12-20 03:56:05'),
(2, 'VII-B', 'Kelas VII-B', NULL, '2021-12-20 03:56:33'),
(3, 'VII-C', 'Kelas VII-C', NULL, '2021-12-20 04:00:23'),
(4, 'VII-D', 'Kelas VII-D', '2021-12-02 18:42:09', '2021-12-20 04:00:58'),
(5, 'VII-E', 'Kelas VII-E', '2021-12-20 03:50:22', '2021-12-20 04:01:17'),
(6, 'VII-F', 'Kelas VII-F', '2021-12-20 03:51:02', '2021-12-20 04:01:39'),
(7, 'VII-G', 'Kelas VII-G', '2021-12-20 03:51:33', '2021-12-20 04:02:06'),
(8, 'VIII-A', 'Kelas VIII-A', '2021-12-20 03:53:21', '2021-12-20 04:02:42'),
(9, 'VIII-B', 'Kelas VIII-B', '2021-12-20 03:53:50', '2021-12-20 04:03:07'),
(10, 'VIII-C', 'Kelas VIII-C', '2021-12-20 03:54:10', '2021-12-20 04:04:01'),
(11, 'VIII-D', 'Kelas VIII-D', '2021-12-20 03:55:07', '2021-12-20 04:04:19'),
(12, 'VIII-E', 'Kelas VIII-E', '2021-12-20 03:55:31', '2021-12-20 04:04:35'),
(13, 'VIII-E', 'Kelas VIII-E', '2021-12-20 04:05:42', '2021-12-20 04:05:42'),
(14, 'VIII-F', 'Kelas VIII-F', '2021-12-20 04:06:21', '2021-12-20 04:06:21'),
(15, 'VIII-G', 'Kelas VIII-G', '2021-12-20 04:06:56', '2021-12-20 04:06:56'),
(16, 'VIII-H', 'Kelas VIII-H', '2021-12-20 04:07:25', '2021-12-20 04:07:25'),
(17, 'VIII-I', 'Kelas VIII-I', '2021-12-20 04:07:44', '2021-12-20 04:07:44'),
(18, 'IX-A', 'Kelas IX-A', '2021-12-20 04:08:15', '2021-12-20 04:08:15'),
(19, 'IX-B', 'Kelas IX-B', '2021-12-20 04:08:34', '2021-12-20 04:08:34'),
(20, 'IX-C', 'Kelas IX-C', '2021-12-20 04:08:50', '2021-12-20 04:08:50'),
(21, 'IX-D', 'Kelas IX-D', '2021-12-20 04:09:17', '2021-12-20 04:09:17'),
(22, 'IX-E', 'Kelas IX-E', '2021-12-20 04:09:33', '2021-12-20 04:09:33'),
(23, 'IX-F', 'Kelas IX-F', '2021-12-20 04:09:51', '2021-12-20 04:09:51'),
(24, 'IX-G', 'Kelas IX-G', '2021-12-20 04:10:12', '2021-12-20 04:10:12'),
(25, 'IX-H', 'Kelas IX-H', '2021-12-20 04:10:27', '2021-12-20 04:10:27'),
(26, 'IX-I', 'Kelas IX-I', '2021-12-20 04:10:42', '2021-12-20 04:10:42'),
(27, 'VII-J', 'Kelas VII-J', '2021-12-21 04:54:00', '2021-12-21 04:54:00'),
(28, 'Lulus', 'Lulus', '2021-12-22 09:41:50', '2021-12-22 09:41:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_mapel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id`, `nama_mapel`, `created_at`, `updated_at`) VALUES
(1, 'Bahasa Indonesia', NULL, '2021-12-19 07:37:39'),
(2, 'Matematika', NULL, NULL),
(3, 'Pendidikan Agama Islam', '2021-12-02 21:29:02', '2021-12-02 21:29:02'),
(4, 'Seni Budaya', '2021-12-02 21:29:39', '2021-12-02 21:29:39'),
(5, 'Bahasa Inggris', '2021-12-09 10:27:20', '2021-12-09 10:27:20'),
(6, 'PPKN', '2021-12-20 04:11:55', '2021-12-20 04:11:55'),
(7, 'IPA', '2021-12-20 04:12:29', '2021-12-20 04:12:29'),
(8, 'IPS', '2021-12-20 04:12:34', '2021-12-20 04:12:34'),
(9, 'PJOK', '2021-12-20 04:12:54', '2021-12-20 04:13:50'),
(10, 'Bahasa Indramayu', '2021-12-20 04:14:35', '2021-12-20 04:14:35'),
(11, 'Prakarya', '2021-12-20 04:14:43', '2021-12-20 04:14:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `materi`
--

CREATE TABLE `materi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mapel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kesimpulan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id_teacher` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `materi`
--

INSERT INTO `materi` (`id`, `mapel`, `kelas`, `judul`, `isi`, `file`, `kesimpulan`, `keterangan`, `user_id_teacher`, `created_at`, `updated_at`) VALUES
(1, 'Matematika', 'XI RPL 1', 'Matriks Ordo 2x2', '            <h1>Matriks Ordo 2x2 - Heading 1</h1>\r\n\r\n            <h2>Heading 2</h2>\r\n\r\n            <p>Ini adalah paragraf, anda tahu apa itu paragraf? Ya, silahkan jawab karena saya tidak tahu, jadi tolong jelaskan.&nbsp;Ini adalah paragraf, anda tahu apa itu paragraf? Ya, silahkan jawab karena saya tidak tahu, jadi tolong jelaskan.Ini adalah paragraf, anda tahu apa itu paragraf? Ya, silahkan jawab karena saya tidak tahu, jadi tolong jelaskan.Ini adalah paragraf, anda tahu apa itu paragraf? Ya, silahkan jawab karena saya tidak tahu, jadi tolong jelaskan.</p>\r\n\r\n            <h3>Ini Numbering - Heading 3&nbsp;:&nbsp;</h3>\r\n\r\n            <ol>\r\n                <li>Ini nomor 1</li>\r\n                <li>Ini nomor 2</li>\r\n                <li>Ini nomor 3</li>\r\n                <li>Dan sayangnya kamu nomor 4 bagi saya.&nbsp;</li>\r\n            </ol>\r\n\r\n            <h2>Heading 2 lagi... cieee.... ^.^</h2>\r\n\r\n            \r\n            ', '', 'hvdsaghvdhgas', 'Ini adalah Keterangan jadi jangan takut gelap.', 2, NULL, NULL),
(2, 'Matematika', 'VII-G', 'Polinomial', '<p>hdbahdw abdwahbdab dwawdbgawvg dwvdvhag</p>', '', 'awda', 'dwadww', 2, '2021-12-02 20:42:05', '2021-12-02 20:42:05'),
(3, 'Matematika', 'VII-G', 'Bangun Ruang', '<p>daasd dahwbdab dawbd&nbsp;</p>', '', 'awdadad', 'asdawdwadawaddaw', 2, '2021-12-02 20:42:53', '2021-12-02 20:42:53'),
(4, 'Matematika', 'VII-G', 'Aljabar', '<p>A+B=C</p>', '', 'msdnjf', NULL, 2, '2021-12-02 20:45:54', '2021-12-02 20:45:54'),
(5, 'Pemprograman Berorientasi Objek', 'VII-G', 'sadsaddsa', '<p>sadjdasjknd</p>', '1639293632_Screenshot (1).png', 'sadsadsa', 'dsadasdsa', 2, '2021-12-12 00:20:32', '2021-12-12 00:20:32'),
(6, 'Matematika', 'VII-G', 'Integral', '<p>dasjhdvahvdjhassbjshdajhsa</p>', '1639293680_materi.pdf', 'sadjhvsajhvbd', 'dashhdvsa', 2, '2021-12-12 00:21:20', '2021-12-12 00:21:20'),
(7, 'Pemprograman Berorientasi Objek', 'XI RPL 3', 'dwdXaasjhsxvsxajhhxx', '<p>hjsavasjhdbajhbdkja</p>', '1639305246_1639293680_materi (1).pdf', 'yugytvb', 'ghvgvg', 2, '2021-12-12 03:34:06', '2021-12-12 03:34:06'),
(8, 'Pemprograman Berorientasi Objek', 'XI RPL 3', 'Polinomial', '<p>r</p>', '1639305531_materi (2).pdf', 'asdsasasaa', 'sadsa', 2, '2021-12-12 03:38:51', '2021-12-12 03:38:51'),
(9, 'Pendidikan Agama Islam', 'VII-G', 'daddwdww', '<p>hvsavdsavhdsah</p>', '1639305851_bandicam 2021-12-10 11-46-57-276.mp4', 'saasasasa', 'dsadsasasad', 2, '2021-12-12 03:44:11', '2021-12-12 03:44:11'),
(10, 'Seni Budaya', 'VII-G', 'ghhxcgsvx', '<p>jhasdvjshajbd shadbab</p>', '1639392996_modul6krn.docx', 'dsaadsasadsa', 'dadsadsds', 2, '2021-12-13 03:56:36', '2021-12-13 03:56:36'),
(11, 'Bahasa Inggris', 'VII-G', 'dsjdhsjfjdsbfjd', '<p>fasbdvajhvdwajhvd</p>', '1639822797_REKON, FK1&FK2_KAB. INDRAMAYU_REKON TAHAP 4 TAHUN 2021 TERMIN 3.xlsx', NULL, NULL, 2, '2021-12-18 10:19:57', '2021-12-18 10:19:57'),
(12, 'Matematika', 'VII-A', 'Bangun Ruang', '<p>Bangun ruang adalah</p>', '1640055230_Proyek 2 Elearning.pptx', 'Bangun ruang memiliki ruang', 'hvdasjhvdasjhvdsa', 30, '2021-12-21 02:53:50', '2021-12-21 02:53:50'),
(13, 'Matematika', 'VII-A', 'Bangun datar', '<p>asgfsghafshgac</p>', '1640062602_codinganiot.docx', 'hfhghgf', 'fhfghfgh', 30, '2021-12-21 04:56:42', '2021-12-21 04:56:42'),
(14, 'Matematika', 'VIII-G', 'Bangun datar', '<p>dsahdsahjvdhsa</p>', '1640062841_modul6krn.docx', 'sajhfdsafv', 'sahdjsah', 30, '2021-12-21 05:00:41', '2021-12-21 05:00:41'),
(15, 'Matematika', 'VIII-A', 'Integral', '<p>fchvhvhv</p>', '1640142750_codinganiot.docx', 'ugggvhggbvh', '7gyb', 30, '2021-12-22 03:12:30', '2021-12-22 03:12:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_12_030653_create_roles_table', 1),
(5, '2019_10_12_030752_create_role_user_table', 1),
(6, '2019_10_16_153041_create_mata_pelajaran_table', 1),
(7, '2019_10_16_153122_create_kelas_table', 1),
(8, '2019_10_17_051051_create_materi_table', 1),
(9, '2019_10_26_035951_create_exercise_table', 1),
(10, '2019_10_26_040125_create_question_table', 1),
(11, '2019_10_26_040201_create_student_answer_table', 1),
(15, '2021_12_14_092151_create_jawaban_tugas_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `question`
--

CREATE TABLE `question` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exercise` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `opt1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opt2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opt3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opt4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct_opt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin Role (Okemin)', NULL, NULL),
(2, 'Teacher', 'Teacher Role', NULL, NULL),
(3, 'Student', 'Student Role', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 3, 3, NULL, NULL),
(4, 3, 4, '2021-11-30 19:11:23', '2021-11-30 19:11:23'),
(5, 3, 5, '2021-12-02 03:20:05', '2021-12-02 03:20:05'),
(6, 3, 6, '2021-12-02 03:25:38', '2021-12-02 03:25:38'),
(7, 3, 7, '2021-12-02 03:32:09', '2021-12-02 03:32:09'),
(8, 3, 8, '2021-12-02 03:35:09', '2021-12-02 03:35:09'),
(9, 3, 9, NULL, NULL),
(10, 3, 10, '2021-12-02 04:07:20', '2021-12-02 04:07:20'),
(11, 3, 11, '2021-12-02 06:54:11', '2021-12-02 06:54:11'),
(12, 2, 12, '2021-12-02 07:13:32', '2021-12-02 07:13:32'),
(13, 1, 13, NULL, NULL),
(14, 3, 14, '2021-12-02 18:44:32', '2021-12-02 18:44:32'),
(15, 3, 28, NULL, NULL),
(16, 3, 29, '2021-12-02 21:17:40', '2021-12-02 21:17:40'),
(17, 2, 13, NULL, NULL),
(18, 2, 30, '2021-12-15 06:05:00', '2021-12-15 06:05:00'),
(19, 2, 31, '2021-12-15 22:52:38', '2021-12-15 22:52:38'),
(20, 3, 32, '2021-12-15 22:55:42', '2021-12-15 22:55:42'),
(21, 3, 33, '2021-12-15 22:59:15', '2021-12-15 22:59:15'),
(22, 3, 34, '2021-12-15 23:01:47', '2021-12-15 23:01:47'),
(23, 2, 35, '2021-12-16 20:18:57', '2021-12-16 20:18:57'),
(24, 3, 37, '2021-12-20 00:15:56', '2021-12-20 00:15:56'),
(25, 3, 38, '2021-12-20 04:26:19', '2021-12-20 04:26:19'),
(26, 3, 39, '2021-12-20 08:09:28', '2021-12-20 08:09:28'),
(27, 3, 40, '2021-12-20 08:12:03', '2021-12-20 08:12:03'),
(28, 3, 41, '2021-12-20 08:14:30', '2021-12-20 08:14:30'),
(29, 2, 42, '2021-12-20 08:23:15', '2021-12-20 08:23:15'),
(30, 2, 43, '2021-12-20 08:25:39', '2021-12-20 08:25:39'),
(31, 3, 44, '2021-12-20 08:28:46', '2021-12-20 08:28:46'),
(32, 1, 46, '2021-12-20 09:38:05', '2021-12-20 09:38:05'),
(33, 1, 47, '2021-12-20 09:46:56', '2021-12-20 09:46:56'),
(34, 1, 48, '2021-12-20 09:49:51', '2021-12-20 09:49:51'),
(35, 1, 49, '2021-12-20 10:05:44', '2021-12-20 10:05:44'),
(36, 3, 50, '2021-12-20 11:19:08', '2021-12-20 11:19:08'),
(37, 1, 51, '2021-12-21 02:06:44', '2021-12-21 02:06:44'),
(38, 3, 52, '2021-12-21 02:10:14', '2021-12-21 02:10:14'),
(39, 2, 53, '2021-12-21 02:12:23', '2021-12-21 02:12:23'),
(40, 3, 54, '2021-12-21 02:37:12', '2021-12-21 02:37:12'),
(41, 1, 55, '2021-12-21 04:47:47', '2021-12-21 04:47:47'),
(42, 3, 56, '2021-12-21 04:50:22', '2021-12-21 04:50:22'),
(43, 2, 57, '2021-12-21 04:52:38', '2021-12-21 04:52:38'),
(44, 1, 66, '2021-12-22 03:07:44', '2021-12-22 03:07:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `student_answer`
--

CREATE TABLE `student_answer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nisn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kelas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_lahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bulan_lahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_lahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_masuk` int(11) DEFAULT NULL,
  `no_telp` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nisn`, `nip`, `name`, `username`, `email`, `kelas`, `jabatan`, `tempat_lahir`, `tgl_lahir`, `bulan_lahir`, `tahun_lahir`, `jenis_kelamin`, `agama`, `tahun_masuk`, `no_telp`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Niken Fijriya', 'Okemin', 'okemin@example.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '087744403036', '$2y$10$1lGbDXa3cLBf1YdtS1QRPOaJgJ/XX/Fl3w3Euw5TWhhICMNgttflK', '1_avatar1640156154.jpg', 'tkqA6Au44fEP3CH07IN0c4o5OgePnpl7Wv1Omx6VkCVBg8y5DgrIbkiMaE03', NULL, '2021-12-22 06:55:54'),
(14, '2003021', NULL, 'Felisa', '2003021', '2003021@gmail.com', 'IX-F', NULL, 'INDRAMAYU', '2001-04-12', '12', '2002', 'P', 'Islam', 2019, '08876756556', 'password', NULL, NULL, '2021-12-02 18:44:32', '2021-12-20 08:19:33'),
(17, NULL, NULL, 'admin', 'admin', 'admin@example.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Z84onrPrpym/9iUfkkd6wOby1i1R/luqD0ewe.4aR78PWkPObHxOK', NULL, NULL, NULL, NULL),
(18, NULL, NULL, 'guru', 'guru', 'guru@example.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$PSYs2dRcLGoXJd0KCN7IMeTQzLkxdArnrwxkyx0haepIZjYt02q66', NULL, NULL, NULL, NULL),
(22, NULL, NULL, 'aan', 'aan', 'aan@example.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$JUj/DezlsEjZHXo9c5YQKOZykRLBxtIWkKJhcJR0xLNl6lAt7R4.S', NULL, NULL, NULL, NULL),
(26, NULL, NULL, 'aana', 'aana', 'aana@example.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$LxZWf/37zMHP0Ggp9hbVJu0WsPLl2h66eiZ45WZaY/Nxzrb3KnKZm', NULL, NULL, NULL, NULL),
(27, NULL, NULL, 'niken', 'nikem', 'niken@example.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$KlL6x7G0RSR.D37baBL9heCXtpGYXSZxldIWkIJerGCvfkkYXybtK', NULL, NULL, NULL, NULL),
(30, NULL, '123456789', 'Aura Rindatama Azzahra', 'guru123', 'guru123@gmail.com', NULL, 'Guru', 'INDRAMAYU', '2021-12-13', NULL, NULL, NULL, 'Islam', NULL, '08876756556', '$2y$10$yX8NI.iJaZ3rF9/i0DBNNeIxl2jjDOshZATh3owsxqmC6Wf6WJ/gy', '30_avatar1640142647.jpg', NULL, '2021-12-15 06:05:00', '2021-12-22 03:10:47'),
(31, NULL, '2000200020', 'Aan Anipah', 'polwan12345', 'aan123@gmail.com', NULL, 'Guru', 'INDRAMAYU', '2021-12-01', NULL, NULL, 'P', 'Islam', NULL, '67767467675', 'password', NULL, NULL, '2021-12-15 22:52:38', '2021-12-20 08:21:09'),
(35, NULL, '73289439827', 'Niken', 'nikenjelek123', 'nikenjelek123@gmail.com', NULL, 'Guru', NULL, '1980-11-28', NULL, NULL, 'P', NULL, NULL, '08999687777', '$2y$10$66NXJvh0Uy5.uRySFP0yhez4LQr5/y9MqwvcpwHzcKnvDWhS5kfHG', NULL, NULL, '2021-12-16 20:18:56', '2021-12-16 20:18:56'),
(36, NULL, '231323', 'adsdjkhdsakhh', 'adadsasad', 'adsdsdsa@gmail.com', NULL, 'Guru', NULL, '2001-11-28', NULL, NULL, NULL, NULL, NULL, '87988778', '$2y$10$MgA2S8NSj1nOHY.l5nFadOmB0xa9v0h4jJVneFAuLyKYIjYxoEkJm', NULL, NULL, '2021-12-17 00:35:30', '2021-12-17 00:35:30'),
(38, '2423423', NULL, 'Aura Rindatama Azzahra', 'aura', 'contoh@gmail.com', 'VII-G', NULL, NULL, '13223-11-28', NULL, NULL, NULL, NULL, 2020, '576575676', '$2y$10$yGZft9NxFruvIlnKAGya.O0RiGAZ7fcRUY2UpVPLVE5.3Chu3R4Ya', NULL, NULL, '2021-12-20 04:26:19', '2021-12-20 08:20:02'),
(39, '2003001', NULL, 'Aan Anipah', '2003016', 'aan@gmail.com', 'VII-A', NULL, NULL, '2002-08-03', NULL, NULL, NULL, NULL, 2020, '087744403036', '$2y$10$uIOQa/iZPr3X68wVlIV.d.1.MfBBjJDyA8MwtjhmkHkk58mVzCAtm', NULL, NULL, '2021-12-20 08:09:28', '2021-12-20 08:18:26'),
(40, '2003022', NULL, 'Niken', 'ken', 'ken04@gmail.com', 'VIII-E', NULL, NULL, '2001-08-04', NULL, NULL, NULL, NULL, 2020, '67767467675', '$2y$10$119EQtshD4wJVbmktPyqEOAAfQdj1KxRWL/MxXqsCSkROHFnMEfVC', NULL, NULL, '2021-12-20 08:12:03', '2021-12-20 08:18:58'),
(41, '2004050', NULL, 'Raga Widatama', 'Raga', 'raga00@gmail.com', 'VII-F', NULL, NULL, '2001-12-20', NULL, NULL, NULL, NULL, 2020, '08876756556', '$2y$10$rQhcJ3R2EsZok3OabtlT5OckyLAiE1rYc7yebrLSsucgAXd9Dtx6S', NULL, NULL, '2021-12-20 08:14:29', '2021-12-20 08:17:14'),
(42, NULL, '2000200010', 'Peter', 'Peter4545', 'peter34@gmail.com', NULL, 'Guru', 'Bandung', '1995-04-20', NULL, NULL, 'L', 'Islam', NULL, '08999687777', '$2y$10$lawF2U2y0rNDcPkanxwGf.Q/UOBijfQbF5qV2nUsJ5a0RbIafT78e', NULL, NULL, '2021-12-20 08:23:15', '2021-12-20 08:23:15'),
(43, NULL, '2000200060', 'Gara Abastian', 'Gara34', 'gara45@gmail.com', NULL, 'Guru', 'Surabaya', '1992-02-14', NULL, NULL, 'L', 'Islam', NULL, '087744403036', '$2y$10$MS4mPRqq8ra2QzRrEnlEwuQAPP2/FM4JosNFj7YFdSM06Z.RXKmrq', NULL, NULL, '2021-12-20 08:25:39', '2021-12-20 08:25:39'),
(44, '2003018', NULL, 'Muhamad Rafli Septian', '2003018', 'rafli@gmail.com', 'IX-I', NULL, 'Kota Tangerang, Kompleks Pondok Bahar, Kecamatan Karang Tengah, Kelurahan Pondok Bahar, Blok B1 No 25', '2002-09-26', NULL, NULL, 'L', 'Islam', 1945, '081411126356', '$2y$10$Rb2xSGPv2KEnPdb/cRdXeOtuNG/ATAKkA2Tp1dupFVVa5HYcw9r7.', NULL, NULL, '2021-12-20 08:28:46', '2021-12-20 08:28:46'),
(45, NULL, NULL, 'Dila Triyani', 'dilaaja', 'dilaaja@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0897689898', '$2y$10$MBBewSDyAGnJa6iTcqCnYumblw9xLuX9awBphmqf77fXF2/coKJvq', NULL, NULL, '2021-12-20 09:35:38', '2021-12-20 09:35:38'),
(46, NULL, NULL, 'Dila Triyani', 'dilaaa', 'dila@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '87968769897', '$2y$10$Kiu1nIX01bjbpl3/Hmn7eunKP4wrwpGOTTEetQwJCIW63c66vSM26', '46_avatar1639996018.jpg', NULL, '2021-12-20 09:38:05', '2021-12-20 10:26:59'),
(47, NULL, NULL, 'gdgsdvsavd', 'hahahaha', 'hahahaha@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '676756453', '$2y$10$9TbYmiD1h/0LcpVuUTL0gOa/YGnwD0.GTR5AXAyLi42WBdlmqvCBG', NULL, NULL, '2021-12-20 09:46:56', '2021-12-20 09:46:56'),
(48, NULL, NULL, 'mdaksdmkaw', 'dkasndksand', 'dnajhg@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '98098768787', '$2y$10$c41L8MfJtX9EBrCQxPQ7aupMbh8g51KezLin/fS/KURJ4P7790o4e', NULL, NULL, '2021-12-20 09:49:50', '2021-12-20 09:49:50'),
(49, NULL, NULL, 'Angel', 'angel', 'angel@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$smTuwUGfKyZWe/kkIXdzKudTKtX8.35kiWFzCF03Y0Sd02qQnnQFe', '49_avatar1639996110.jpg', NULL, '2021-12-20 10:05:43', '2021-12-20 10:28:30'),
(50, '123412312', NULL, 'siswa', 'siswa', 'siswa@gmail.com', 'VIII-G', NULL, 'Indramayu', '2001-11-28', NULL, NULL, 'P', 'Islam', 2020, 'l767887876t87', '$2y$10$Y3xhaQwmtyYhSGprMXKxOusqfl9ghTdnLrCte4tQn3Uwi5L6jMQYC', '50_avatar1640155551.jpg', NULL, '2021-12-20 11:19:08', '2021-12-22 06:45:51'),
(51, NULL, NULL, 'Aan Anipah', 'aananipah3', 'aananipah3@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$/yPzx0h3npj5brrjuhFJduZFOdTaNG45lErc7c/e/Gw1VRM5vi0MS', '51_avatar1640230896.jpg', NULL, '2021-12-21 02:06:44', '2021-12-23 03:41:37'),
(52, '675231675', NULL, 'lana', 'siswa2', 'siswa2@gmail.com', 'VII-A', NULL, NULL, '2008-11-22', NULL, NULL, NULL, NULL, 2021, '634726342', '$2y$10$WglzT0KKxWeKeIbmSj96peZ3/t4RB69zJsIQx7fjz2FeWknU63Fji', NULL, NULL, '2021-12-21 02:10:13', '2021-12-21 02:10:13'),
(53, NULL, '4324323234', 'Bintang', 'bintang', 'bintang@gmail.com', NULL, 'Guru', NULL, '1996-11-28', NULL, NULL, NULL, NULL, NULL, '6875856765', '$2y$10$LcB/j5vBhWBPo3c1lklCLuG.q/TBHXpM7EQqVxd8X.LSG17jFbM9G', NULL, NULL, '2021-12-21 02:12:23', '2021-12-21 02:12:23'),
(54, '3123213', NULL, 'saSa', 'faisal', 'faisal@gmail.com', 'VII-A', NULL, NULL, '2009-11-22', NULL, NULL, NULL, NULL, 2021, NULL, '$2y$10$gX9MqgVvCUuYDefaEs0.vOhze3StqRv5swqVj3Hv7wE1Zg8Z2/Uwi', NULL, NULL, '2021-12-21 02:37:10', '2021-12-21 02:37:10'),
(55, NULL, NULL, 'Aura', 'aurarindatama', 'aurarcontoh@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$b7J6lb2eKwEVpi3bXsxsa.iLs7KT2JzydrprotDbzEc8GZPpWER2C', NULL, NULL, '2021-12-21 04:47:47', '2021-12-21 04:47:47'),
(56, '2003040', NULL, 'aan', 'aananipah4', 'anipah4@gmail.com', 'VIII-G', NULL, NULL, '2001-12-12', NULL, NULL, NULL, NULL, 2021, '08999687777', '$2y$10$6IOSFCQIVshtg6609l6O5OX5A.wDdHZvXTBtG32PNrsDjXMrZrLZe', NULL, NULL, '2021-12-21 04:50:22', '2021-12-21 04:50:22'),
(57, NULL, '5644643435', 'Rohmani', 'rohmani', 'rohmani@gmail.com', NULL, 'Guru', NULL, '2000-07-02', NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Eu8nB1IUct2ghLU.g.wuyObgBKo5bubImHk11qOyEUbo.ET3PbKY6', NULL, NULL, '2021-12-21 04:52:38', '2021-12-21 04:52:38'),
(58, 'Felisa', NULL, 'hashd@gmail.com', 'IX-F', NULL, 'INDRAMAYU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2019, NULL, '$2y$10$lRKgNxxokMC5ufeA9t3UBOO4WrJMgHkrFFY3Nq8Qace6x9qw4OvLW', NULL, NULL, '2021-12-21 16:15:17', '2021-12-21 16:15:17'),
(59, '25376153', NULL, 'Felisa', 'dsahdsav', 'hashd@gmail.com', 'IX-F', NULL, NULL, '2001-04-12', NULL, NULL, NULL, NULL, 2019, NULL, '$2y$10$HVDSVPNhC5VtEP2rF2EWF.V3nuQgYUBD81HYa1FY3a.u2R6t0KQTa', NULL, NULL, '2021-12-21 16:21:08', '2021-12-21 16:21:08'),
(62, '5435435', NULL, 'Felisa', 'dfsd', 'adsd@gmail.com', 'IX-F', NULL, NULL, '2001-04-12', NULL, NULL, NULL, NULL, 2019, NULL, '$2y$10$OHUx6U.M8jXOBVmYMSwkuu58OcZinyYkpOxHydaEQ16V33oer8NYu', NULL, NULL, '2021-12-21 16:24:53', '2021-12-21 16:24:53'),
(66, NULL, NULL, 'Lana', 'lana123', 'lana@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$nOPEZ/i8dRHShf56SVBTbeEOxe.C8m06wwKU5VFDJpPL6uvKu7ee.', NULL, NULL, '2021-12-22 03:07:44', '2021-12-22 03:07:44'),
(72, '123213', NULL, 'Felisa', 'dsadsaa12', 'ghjasj@gmail.com', 'IX-F', NULL, NULL, '2001-04-12', NULL, NULL, NULL, NULL, 2019, NULL, '$2y$10$424yl0fFH.2G04MJj6aBD.xU84XMfHAgZCyVG3D7sKIpem5rd4/Cy', NULL, NULL, '2021-12-23 02:39:03', '2021-12-23 02:39:03'),
(73, '879789', NULL, 'Felisa', 'ygvad', 'njnjjkn@gmail.com', 'IX-F', NULL, NULL, '2001-04-12', NULL, NULL, NULL, NULL, 2019, NULL, '$2y$10$hWA4UPsNvblh3qeoVmhViOrXY/6SEeYHOEcn0gNzLm.9bUTi.aL3y', NULL, NULL, '2021-12-23 02:40:32', '2021-12-23 02:40:32');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `exercise`
--
ALTER TABLE `exercise`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jawaban_tugas`
--
ALTER TABLE `jawaban_tugas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jawaban_tugas_id_mapel_foreign` (`id_mapel`),
  ADD KEY `jawaban_tugas_id_exercise_foreign` (`id_exercise`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `student_answer`
--
ALTER TABLE `student_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_nisn_unique` (`nisn`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `exercise`
--
ALTER TABLE `exercise`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `jawaban_tugas`
--
ALTER TABLE `jawaban_tugas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `materi`
--
ALTER TABLE `materi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `question`
--
ALTER TABLE `question`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `student_answer`
--
ALTER TABLE `student_answer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `jawaban_tugas`
--
ALTER TABLE `jawaban_tugas`
  ADD CONSTRAINT `jawaban_tugas_id_exercise_foreign` FOREIGN KEY (`id_exercise`) REFERENCES `exercise` (`id`),
  ADD CONSTRAINT `jawaban_tugas_id_mapel_foreign` FOREIGN KEY (`id_mapel`) REFERENCES `mata_pelajaran` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
